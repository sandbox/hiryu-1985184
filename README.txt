
Webform Advanced Report gives you a better way to generate report from webform
submissions.

How it works
------------

You can include user data from user profile, and not just the user name like the
original webform report feature, included custom user fields generated with
Fields module.

Possibility to select multiple webform, coming from different content types, for
one single report. As any webform should have a different structure, the module
will format the data shifting the empty cells to let data appear under the right
column.

Select an entity field, for every content checked for report generation, to act
as filter in report page. This is a useful option if your webforms content types
have an entity field where they are grouped by, so you can easily choose from
wich nodes you want to generate report for just selecting the parent node.

It's also possible to partialize webform and user fields to exclude unwanted
fields from the report, filter your results by a specific date range, and,
obviously, export the result data in CSV, XML or HTML.

Installation
------------

Copy webform_advanced_report folder to your module directory and then
enable on the admin modules page. You can set your options type at
admin/config/webformadvancedreport/settings.

Author
------
Gianluca Agnocchetti (hiryu)
hiryu@gmx.com
