<?php

/**
 * @file
 * Generate better and complete reports from webform submissions
 *
 * Webform Advanced Report
 *
 * Drupal 7 version:
 * Written by Gianluca Agnocchetti (hiryu)
 *
 * http://drupal.org/user/2502794
 */

/**
 * Definition of constants used in the module.
 */
define('WEBFORM_ADVANCED_REPORT_DEFAULT_START_DATE', '1980-02-18 13:00');
define('WEBFORM_ADVANCED_REPORT_DEFAULT_END_DATE', '2038-01-19 00:00');
define('WEBFORM_ADVANCED_REPORT_TABLE_NAME', 'webform_advanced_report_marked_submissions');
