<?php

/**
 * @file
 * Generate better and complete reports from webform submissions
 *
 * Webform Advanced Report
 *
 * Drupal 7 version:
 * Written by Gianluca Agnocchetti (hiryu)
 *
 * http://drupal.org/user/2502794
 */

/**
 * Retriving all content types.
 */
function webform_advanced_report_get_node_types() {
  $results = node_type_get_types();
  foreach ((array) $results as $key => $type) {
    $types[$key] = $type->name;
  }
  return $types;
}

/**
 * Sort multidimensional array.
 */
function webform_advanced_report_msort($array, $key, $sort_flags = SORT_REGULAR, $sort_type = 'SORT_ASC') {
  if (is_array($array) && count($array) > 0) {
    if (!empty($key)) {
      $mapping = array();
      foreach ($array as $k => $v) {
        $sort_key = '';
        if (!is_array($key)) {
          $sort_key = $v[$key];
        }
        else {
          foreach ($key as $key_key) {
            $sort_key .= $v[$key_key];
          }
          $sort_flags = SORT_STRING;
        }
        $mapping[$k] = $sort_key;
      }
      asort($mapping, $sort_flags);
      $sorted = array();
      foreach ($mapping as $k => $v) {
        $sorted[$k] = $array[$k];
      }
      if ($sort_type == 'SORT_DESC') {
        $sorted = array_reverse($sorted);
      }
      return $sorted;
    }
  }
  return $array;
}

/**
 * Recursively searches a multidimensional array for a key and optional value.
 */
function webform_advanced_report_multi_array_key_search($needle, $haystack, $strict = FALSE, $output = 'array', $value = NULL) {
  // Sanity Check.
  if (!is_array($haystack)) {
    return FALSE;
  }

  $res_idx  = 'matchedIdx';
  $prev_key = "";
  $keys     = array();

  $numargs = func_num_args();
  if ($numargs > 5) {
    $arg_list = func_get_args();
    $keys     = $arg_list[5];
    $prev_key = $arg_list[6];
  }

  $keys[$res_idx] = isset($keys[$res_idx]) ? $keys[$res_idx] : 0;

  foreach ($haystack as $key => $val) {
    if (is_array($val)) {
      if ((($key === $needle) && is_null($value)) || (($key === $needle) && ($val[$key] == $value) && $strict === FALSE) || (($key === $needle) && ($val[$key] === $value) && $strict === TRUE)) {
        if ($output == 'value') {
          $keys[$keys[$res_idx]] = $val;
        }
        else {
          $keys[$keys[$res_idx]] = $prev_key . (isset($keys[$keys[$res_idx]]) ? $keys[$keys[$res_idx]] : "") . "[\"$key\"]";
        }
        $keys[$res_idx]++;
      }
      $passed_key = $prev_key . "[\"$key\"]";
      $keys = webform_advanced_report_multi_array_key_search($needle, $val, $strict, $output, $value, $keys, $passed_key);
    }
    else {
      if ((($key === $needle) && is_null($value)) || (($key === $needle) && ($val == $value) && $strict === FALSE) || (($key === $needle) && ($val === $value) && $strict === TRUE)) {
        if ($output == 'value') {
          $keys[$keys[$res_idx]] = $val;
        }
        else {
          $keys[$keys[$res_idx]] = $prev_key . (isset($keys[$keys[$res_idx]]) ? $keys[$keys[$res_idx]] : "") . "[\"$key\"]";
        }
        $keys[$res_idx]++;
      }
    }
  }
  if ($numargs < 6) {
    $keys['num_matches'] = (count($keys) == 1) ? 0 : $keys[$res_idx];
    unset($keys[$res_idx]);
    if (($output == 'array') && $keys['num_matches'] > 0) {
      $arr_keys = array(
        'num_matches' => $keys['num_matches'],
      );
      for ($i = 0; $i < $keys['num_matches']; $i++) {
        $keys_arr = explode(',', str_replace(array(
          '][',
          '[',
          ']',
        ), array(
          ',',
          '',
          '',
        ), $keys[$i]));
        $json    = "";
        foreach ($keys_arr as $nestedkey) {
          $json .= "{" . $nestedkey . ":";
        }
        $json .= is_null($value) ? "\"\"" : "\"$value\"";
        foreach ($keys_arr as $nestedkey) {
          $json .= "}";
        }
        $arr_keys[$i] = json_decode($json, TRUE);
      }
      $keys = $arr_keys;
    }
  }
  return $keys;
}
