<?php

/**
 * @file
 * Generate better and complete reports from webform submissions
 *
 * Webform Advanced Report
 *
 * Drupal 7 version:
 * Written by Gianluca Agnocchetti (hiryu)
 *
 * http://drupal.org/user/2502794
 */

module_load_include('inc', 'webform_advanced_report', 'includes/webform_advanced_report.common');
module_load_include('inc', 'webform_advanced_report', 'includes/webform_advanced_report.functions');

/**
 * Implements hook_schema().
 */
function webform_advanced_report_schema() {
  if (!db_table_exists(WEBFORM_ADVANCED_REPORT_TABLE_NAME)) {
    $schema[WEBFORM_ADVANCED_REPORT_TABLE_NAME] = array(
      'fields' => array(
        'sid' => array(
          'type' => 'int',
          'length' => 10,
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'mark' => array(
          'type' => 'int',
          'length' => 1,
          'not null' => FALSE,
          'default' => 0,
        ),
      ),
      'primary key' => array('sid'),
    );
    return $schema;
  }
  else {
    return;
  }
}

/**
 * Implements hook_install().
 */
function webform_advanced_report_install() {
  // Set default date variables.
  variable_set('webform_advanced_report_user_date_start', WEBFORM_ADVANCED_REPORT_DEFAULT_START_DATE);
  variable_set('webform_advanced_report_user_date_end', WEBFORM_ADVANCED_REPORT_DEFAULT_END_DATE);
  variable_set('webform_advanced_report_user_personalize_user_fields', NULL);
  variable_set('webform_advanced_report_user_orderby', 'SORT_ASC');
}

/**
 * Implements hook_uninstall().
 */
function webform_advanced_report_uninstall() {
  // Drop db table.
  // If you want to drop table on uninstall just add:
  // drupal_uninstall_schema('webform_advanced_report');
  // Unset variables.
  variable_del('webform_advanced_report_setup_show_nid');
  variable_del('webform_advanced_report_setup_ct');
  variable_del('webform_advanced_report_setup_empty_char');
  variable_del('webform_advanced_report_setup_mark_label');
  variable_del('webform_advanced_report_setup_date_pattern');
  variable_del('webform_advanced_report_setup_max_fields_len');
  variable_del('webform_advanced_report_user_personalize_user_fields');
  variable_del('webform_advanced_report_user_personalize_hide_marked');
  variable_del('webform_advanced_report_user_personalize_wf');
  variable_del('webform_advanced_report_user_nids');
  variable_del('webform_advanced_report_setup_show_op_cols');
  variable_del('webform_advanced_report_user_date_start');
  variable_del('webform_advanced_report_user_date_end');
  variable_del('webform_advanced_report_user_sort');
  variable_del('webform_advanced_report_user_orderby');
  variable_del('webform_advanced_report_get_webform_fields');
  $types = webform_advanced_report_get_node_types();
  foreach ($types as $key => $type) {
    variable_del('webform_advanced_report_setup_ct_ref_' . $key);
  }
}
