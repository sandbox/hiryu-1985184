/**
 * @file
 * Generate better and complete reports from webform submissions
 *
 * Webform Advanced Report
 *
 * Drupal 7 version:
 * Written by Gianluca Agnocchetti (hiryu)
 *
 * http://drupal.org/user/2502794
 */

// Enable/disable submit buttons.
function check_report_submit() {
  if (jQuery('.form-item-webform-advanced-report-user-nids select').val()) {
    jQuery('#webform-advanced-report-user-form .submit-generate').attr('disabled',false);
    jQuery('#webform-advanced-report-user-form .submit-generate').css({ opacity: 1 });
  }
  else {
    jQuery('#webform-advanced-report-user-form .submit-generate').attr('disabled',true);
    jQuery('#webform-advanced-report-user-form .submit-generate').css({ opacity: 0.60 });
  }
}

jQuery(document).ready(function() {
  // Show/hide filter.
  jQuery('#webform_advanced_report_hide_filter_link').click(function() {
    jQuery('.form-item-webform-advanced-report-user-entity-refs').slideToggle('fast');
    jQuery('#edit-webform-advanced-report-user-entity-refs').val(0);
    jQuery('#edit-webform-advanced-report-user-entity-refs').trigger('change');
  });

  // Reset nids on refs change.
  jQuery('#edit-webform-advanced-report-user-entity-refs').change(function() {
    if (jQuery('#edit-webform-advanced-report-user-entity-refs').val() != '0') {
      jQuery('.form-item-webform-advanced-report-user-nids select').val(null);
      check_report_submit();
    }
  });

  // Disable and change text on submit button.
  jQuery('#edit-webform-advanced-report-user-submit').click(function() {
    jQuery('#webform_advanced_report_user_loading_image').show();
    jQuery(this).val('Loading...');
    jQuery(this).css({ opacity: 0.60 });
    jQuery(this).attr('disabled', 'disabled');
    jQuery(this).parents('form').submit();
  });

  // Update generate report button text on option change.
  jQuery('.form-item-webform-advanced-report-user-nids select').live("change", function() {
    jQuery('#edit-webform-advanced-report-user-submit').val('Generate Report');
  });

  // Check if at least one node is selected for report.
  check_report_submit();
  jQuery('.form-item-webform-advanced-report-user-nids select').live("change", check_report_submit);

  // Clear dates input.
  jQuery('#webform_advanced_report_user_date_clear').click(function() {
    jQuery('#edit-webform-advanced-report-user-date-range input').val('');
  });
});
